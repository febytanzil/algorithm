package com.feby.main;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearch {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int[] whitelist = new int[10];
		int i = 0;
		
		while (i < 10) {
			whitelist[i] = scanner.nextInt();
			i++;
		}
		
		Arrays.sort(whitelist);
		System.out.println(Arrays.toString(whitelist));
		
		do {
			int key = scanner.nextInt();
			
			System.out.println(rank(key, whitelist));
		} while (true);
	}
	
	public static int rank(int key, int[] a) {
		int lo = 0;
		int hi = a.length - 1;
		
		while (lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (key < a[mid]) {
				hi = mid - 1;
			} else if (key > a[mid]) {
				lo = mid + 1;
			} else {
				return mid;
			}
		}
		
		return -1;
	}
}
