package com.feby.main.seri2;

import java.util.Scanner;

public class PrimeSum {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		long input = sc.nextLong();
		
		System.out.println(primeSum(input));
		
		sc.close();
	}
	
	private static long primeSum(long limit) {
		long result = 0;
		
		for (long i = 0; i < limit; i++) {
			if (isPrime(i)) {
				result += i;
			}
		}
		
		return result;
	}
	
	public static boolean isPrime(long n) {
	    if (n <= 3) {
	        return n > 1;
	    } else if (n % 2 == 0 || n % 3 == 0) {
	        return false;
	    } else {
	        for (int i = 5; i < Math.sqrt(n) + 1; i += 6) {
	            if (n % i == 0 || n % (i + 2) == 0) {
	                return false;
	            }
	        }
	        return true;
	    }
	}
}
