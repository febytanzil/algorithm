package com.feby.main.seri2;

import java.util.Scanner;

public class FactorialSum {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		long input = sc.nextLong();
		long sum = 0;
		
		char[] numbers = Long.toString(factorial(input)).toCharArray();
		
		for (int i = 0; i < numbers.length; i++) {
			sum = sum + new Long(Character.toString(numbers[i]));
		}
		
		System.out.println(sum);
		
		sc.close();
	}
	
	public static long factorial(long n) {
		if (1 == n) {
			return 1;
		}
		
		return n * factorial(n - 1);
	}
}
