package com.feby.main;

import java.util.Scanner;

public class BinaryReverse {
	static int N;
	static int Answer;

	public static void main(String args[]) throws Exception
	{
		/*
		   The method below means that the program will read from input.txt, instead of standard(keyboard) input.
		   To test your program, you may save input data in input.txt file,
		   and call below method to read from the file when using nextInt() method.
		   You may remove the comment symbols(//) in the below statement and use it.
		   But before submission, you must remove the freopen function or rewrite comment symbols(//).
		 */
		// System.setIn(new FileInputStream("input.txt"));

		/*
		   Make new scanner from standard input System.in, and read data.
		 */
		Scanner sc = new Scanner(System.in);

		for(int test_case = 1; test_case <= 10; test_case++)
		{
			/*
				Read the test case from standard input.		 
			*/
			N = sc.nextInt();


			/////////////////////////////////////////////////////////////////////////////////////////////
			/*
			   Implement your algorithm here.
			 */
			/////////////////////////////////////////////////////////////////////////////////////////////
			String binary = Integer.toBinaryString(N);
			int j = 0;
			Answer = 0;
			for (int i = 0; i < binary.length(); i++) {
				Answer += ('0' == binary.charAt(i) ? 0 : 1) * (Math.pow(2, j));
				j++;
			}

			// Print the answer to standard output(screen).
			System.out.println("#"+test_case+" "+Answer);
		}
		System.out.println();
	}
}
