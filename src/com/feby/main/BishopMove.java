package com.feby.main;

import java.util.Scanner;

public class BishopMove {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bishop Move Calculator");
		
		System.out.print("Enter value for r1 = ");
		int r1 = scanner.nextInt();
		System.out.print("Enter value for c1 = ");
		int c1 = scanner.nextInt();
		System.out.print("Enter value for r2 = ");
		int r2 = scanner.nextInt();
		System.out.print("Enter value for c2 = ");
		int c2 = scanner.nextInt();
		
		scanner.close();
		
		long start = System.currentTimeMillis();
		System.out.println("start");
		int result = howManyMoves(r1, c1, r2, c2);
		long end = System.currentTimeMillis();
		
		System.out.printf("Returns = %d, process time = %d ms", result, end - start);
	}
	
	private static int howManyMoves(int r1, int c1, int r2, int c2) {
		int result = 0;
		
		if (!isSameColor(r1, c1, r2, c2)) {
			return -1;
		} else if (r1 == r2 && c1 == c2) {
			return result;
		}
		
		if ((r2 - r1) <= 7 && (r2 - r1) >= 0 && (c2 - c1) <= 7 && (c2 - c1) >= 0 && (r2 - r1) == (c2 - c1)) {
			System.out.println("dari atas ke kanan bawah");
			// dari atas ke kanan bawah
			result = 1;
		} else if ((r1 - r2) <= 7 && (r1 - r2) >= 0 && (c2 + c1) <= 14 && (c2 + c1) >= 0 && (r1 - r2) == (c2 + c1)) {
			System.out.println("dari bawah ke kiri atas");
			// dari bawah ke kiri atas 
			result = 1;
		} else if ((r2 + r1) <= 14 && (r2 + r1) >= 0 && (c1 - c2) <= 7 && (c2 - c1) >= 0 && (r2 + r1) == (c1 - c2)) {
			System.out.println("dari atas ke kiri bawah");
			// dari atas ke kiri bawah
			result = 1;
		} else if ((r2 + r1) <= 14 && (r2 + r1) >= 0 && (c2 + c1) <= 7 && (c2 + c1) >= 0 && (r1 - r2) == (c2 - c1)) {
			System.out.println("dari bawah ke kanan atas");
			// dari bawah ke kanan atas
			result = 1;
		} else {
			//TODO
			int[] arr = new int[4];
			
			System.out.printf("Trying to move from (%d, %d) to (%d, %d) %n", 7 - r1, 7 - c1, r2, c2);
			arr[0] = result + howManyMoves(7 - r1, 7 - c1, r2, c2);
			System.out.printf("Trying to move from (%d, %d) to (%d, %d) %n", r1 - 7, c1 - 7, r2, c2);
			arr[1] = result + howManyMoves(r1 - 7, c1 - 7, r2, c2);
			System.out.printf("Trying to move from (%d, %d) to (%d, %d) %n", 7 - r1, c1 - 7, r2, c2);
			arr[2] = result + howManyMoves(7 - r1, c1 - 7, r2, c2);
			System.out.printf("Trying to move from (%d, %d) to (%d, %d) %n", r1 - 7, 7 - c1, r2, c2);
			arr[3] = result + howManyMoves(r1 - 7, 7 - c1, r2, c2);
			
			result = getMax(arr);
		}
		
		return result;
	}
	
	private static boolean isSameColor(int r1, int c1, int r2, int c2) {
		if (getColor(r1, c1) == getColor(r2, c2)) {
			return true;
		}
		
		return false;
	}
	
	private static int getMax(int[] arr) {
		int result = -2;
		
		for (int i = 0; i < arr.length; i++) {
			if (result < arr[i]) {
				result = arr[i];
			}
		}
		
		return result;
	}
	
	/**
	 * @param r
	 * @param c
	 * @return true if white, false if black
	 */
	private static boolean getColor(int r, int c) {
		if (c % 2 == 0) {
			if (r % 2 == 0) {
				return true;
			} else {
				return false;
			}
		} else {
			if (r % 2 == 0) {
				return false;
			} else {
				return true;
			}
		}
	}
	
}
