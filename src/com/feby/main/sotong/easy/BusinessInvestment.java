package com.feby.main.sotong.easy;

import java.util.Scanner;

public class BusinessInvestment {
	private static int money = 0;
	private static int totalInvestment = 0;
	private static int totalMoney = 0;
	private static int max = 0;
	private static int company = 0;
	private static int[][][] matrix;
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//sc = new Scanner(new FileInputStream("input.txt"));

		int T = sc.nextInt();
		matrix = new int[T][][];
		
		for (int tc = 0; tc < T; tc++) {
			money = sc.nextInt();
			company = sc.nextInt();
			matrix[tc] = new int[money][company];
			
			for (int i = 0; i < money; i++) {
				sc.nextInt();
				for (int j = 0; j < company; j++) {
					matrix[tc][i][j] = sc.nextInt();
				}
			}
		}
		
		for (int i = 0; i < T; i++) {
			for (int j = 0; j < company; j++) {
				if (max < matrix[i][money - 1][j]) {
					max = matrix[i][money - 1][j];
				}
			}
			sum(company - 1, 0, 0, i);
			System.out.println(max);
			max = 0;
		}
		
		sc.close();
	}
	
	private static void sum(int company, int previousMoney, int previousInvestment, int tCase) {
		if (0 > company) {
			//System.out.println("Sudah membeli semua company.");
			if (money < totalMoney) {
				totalInvestment -= previousInvestment;
				totalMoney -= previousMoney;
				/*System.out.println("Overbudget");
				System.out.println("Total uang sementara kita kembalikan menjadi " + totalMoney + " won");
				System.out.println("Hasil investasi sementara kita kembalikan menjadi " + totalInvestment + " won");
				System.out.println();*/
			} else if (max < totalInvestment) {
				max = totalInvestment;
				totalInvestment -= previousInvestment;
				totalMoney -= previousMoney;
				/*System.out.println("Max baru ditemukan = " + max);
				System.out.println("Total uang sementara kita kembalikan menjadi " + totalMoney + " won");
				System.out.println("Hasil investasi sementara kita kembalikan menjadi " + totalInvestment + " won");
				System.out.println();*/
			}
		} else {
			for (int i = 0; i < money; i++) {
				//System.out.println("Sekarang kita menilik company " + company);
				//System.out.println(String.format("Hasil investasi jika invest %d won di company %d adalah %d won", i + 1, company, matrix[i][company]));
				totalInvestment += matrix[tCase][i][company];
				//System.out.println(String.format("Hasil investasi sementara setelah invest %d won di company %d adalah %d won", i + 1, company, totalInvestment));
				totalMoney += (i + 1);
				//System.out.println(String.format("Total uang setelah invest %d won di company %d adalah %d won", i + 1, company, totalMoney));
				sum(company - 1, (i + 1), matrix[tCase][i][company], tCase);
				if (BusinessInvestment.company == company + 1) {
					totalInvestment = totalMoney = 0;
				}
			}
		}
	}
}
