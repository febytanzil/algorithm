package com.feby.main.sotong.easy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class PermutationCheerleaders {
	private static SortedSet<String> result = new TreeSet<>();
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		List<String> cases = new ArrayList<String>();

		int T = sc.nextInt();
		for(int tc = 0; tc < T; tc++) {
			cases.add(sc.next());
		}
		
		for (String c : cases) {
			permute("", c);
			for (String res : result) {
				System.out.println(res);
			}
			System.out.println();
			result.clear();
		}
		
		sc.close();
	}
	
	private static void permute(String prefix, String str) {
		int n = str.length();
		
		if (0 == n) {
			result.add(prefix);
		} else {
			for (int i = 0; i < n; i++) {
				permute(prefix + str.charAt(i), str.substring(0, i) + str.substring(i + 1, n));
			}
		}
	}
}
