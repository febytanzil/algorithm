package com.feby.main.sotong.easy;

import java.util.Scanner;

public class DiscriminatingPassword {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//sc = new Scanner(new FileInputStream("input.txt"));

		int T = sc.nextInt();
		String[] arr = new String[T];
		
		for(int tc = 0; tc < T; tc++) {
			arr[tc] = sc.next();
		}
		
		for(int tc = 0; tc < T; tc++) {
			checkPassword(arr[tc]);
		}
		
		sc.close();
	}
	
	private static void checkPassword(String str) {
		System.out.println(str);
		if (str.matches(".*([A-Z])(\\1+).*")) {
			System.out.println("Rejected");
		} else {
			System.out.println("Accepted");
		}
	}
}
