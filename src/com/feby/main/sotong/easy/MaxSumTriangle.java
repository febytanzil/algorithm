package com.feby.main.sotong.easy;

import java.util.Scanner;

public class MaxSumTriangle {
	private static int[][][] triangles;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//sc = new Scanner(new FileInputStream("input.txt"));

		int T = sc.nextInt();
		triangles = new int[T][][];
		
		for (int tc = 0; tc < T; tc++) {

			/**********************************
			*  Implement your algorithm here. *
			***********************************/
			int size = sc.nextInt();
			triangles[tc] = new int[size][];
			
			for (int i = 0; i < size; i++) {
				triangles[tc][i] = new int[i + 1];
				
				for (int j = 0; j < (i + 1); j++) {
					triangles[tc][i][j] = sc.nextInt();
				}
			}
			
			// Print the answer to standard output(screen).

		}
		
		for (int tc = 0; tc < T; tc++) {
			System.out.println(calculate(triangles[tc]));
		}
		
		sc.close();
	}
	
	private static int calculate(int[][] triangle) {
		
		for (int i = triangle.length - 1; i >= 0; i--) {
			
			for (int j = 0; j < triangle[i].length - 1; j++) {
				int left = triangle[i][j] + triangle[i - 1][j];
				int right = triangle[i][j + 1] + triangle[i - 1][j];
				
				if (left > right) {
					triangle[i - 1][j] = left;
				} else {
					triangle[i - 1][j] = right;
				}
			}
		}
		
		return triangle[0][0];
	}

}
