package com.feby.main.seri3;

import java.util.Scanner;

public class PalindromeCounter {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println(countPalindrome(sc.nextInt()));
		
		sc.close();
	}
	
	private static int countPalindrome(int input) {
		int result = 0;
		
		for (int i = 10; i <= input; i++) {
			String normal = Integer.toString(i);
			String reverse = new StringBuilder(normal).reverse().toString();
			
			if (normal.equals(reverse)) {
				result++;
			}
		}
		
		return result;
	}
}
