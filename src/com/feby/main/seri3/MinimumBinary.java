package com.feby.main.seri3;

import java.util.Scanner;

public class MinimumBinary {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println(generateMin(sc.nextInt()));
		
		sc.close();
	}
	
	private static int generateMin(int input) {
		if (1 == input) {
			return 0;
		}
		
		char[] seq = new char[input];
		seq[0] = '1';
		
		for (int i = 1; i < input; i++) {
			seq[i] = '0';
		}
		
		String result = new String(seq);
		
		return Integer.parseInt(result, 2);
	}
}
