package com.feby.main;

public class Euclids {
	public static void main(String[] args) {
		System.out.println(greatestCommonDivisor(1111111, 1234567));
		System.out.println(('a' + 'b'));
	}
	
	public static long greatestCommonDivisor(long a, long b) {
		System.out.println(a + " - " + b);
		if (0 == b) {
			return a;
		}
		
		long remainder = a % b;
		
		return greatestCommonDivisor(b, remainder);
	}
}
