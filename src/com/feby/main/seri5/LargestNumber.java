package com.feby.main.seri5;

import java.io.IOException;
import java.util.Scanner;

public class LargestNumber {
	private static long start;

	public static void main(String[] args) throws IOException {
		StringBuilder sb = new StringBuilder();
		Scanner sc = new Scanner(System.in);
		String line = null;

		while (null != (line = sc.nextLine())) {
			sb.append(line);
			
			if (null == line || line.isEmpty()) {
				break;
			}
		}

		//start = System.currentTimeMillis();
		System.out.println(largestMul(sb.toString()));
		
		sc.close();
	}
	
	private static int largestMul(String s) {
		int temp = 0;
		int max = 0;
		
		for (int i = 0; i < 995; i++) {
			temp = mul(s.substring(i, i + 5).toCharArray());
			
			if (max < temp) {
				max = temp;
			}
		}
		
		//System.out.println(System.currentTimeMillis() - start);
		return max;
	}
	
	private static int mul(char[] arr) {
		return Character.digit(arr[0], 10) * Character.digit(arr[1], 10) * Character.digit(arr[2], 10) * Character.digit(arr[3], 10) * Character.digit(arr[4], 10); 
	}

}
