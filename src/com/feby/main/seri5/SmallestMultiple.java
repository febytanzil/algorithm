package com.feby.main.seri5;

public class SmallestMultiple {
	private static final int MAX = 20;

	public static void main(String[] args) {
		long result = 1;
		
		for (int i = 1; i <= MAX; i++) {
			if (isPrime(i)) {
				result *= i;
			}
		}
		
		for (int i = 2; i <= MAX; i++) {
			if (0 != result % i) {
				if (0 == i % 2) {
					result *= 2;
				} else if (0 == i % 3) {
					result *= 3;
				} else if (0 == i % 5) {
					result *= 5;
				} else if (0 == i % 7) {
					result *= 7;
				}
			}
		}
		
		System.out.println(result);
	}
	
	public static boolean isPrime(long n) {
	    if (n <= 3) {
	        return n > 1;
	    } else if (n % 2 == 0 || n % 3 == 0) {
	        return false;
	    } else {
	        for (int i = 5; i < Math.sqrt(n) + 1; i += 6) {
	            if (n % i == 0 || n % (i + 2) == 0) {
	                return false;
	            }
	        }
	        return true;
	    }
	}

}
