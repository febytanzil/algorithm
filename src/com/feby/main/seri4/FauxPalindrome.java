package com.feby.main.seri4;

import java.util.Scanner;

public class FauxPalindrome {
	private static final String PALINDROME = "PALINDROME";
	private static final String FAUX = "FAUX";
	private static final String NOT_EVEN = "NOT EVEN FAUX";

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println(classifyIt(sc.next()));
		
		sc.close();
	}
	
	private static String classifyIt(String text) {
		StringBuilder sb = new StringBuilder(text);
		
		if (sb.reverse().toString().equals(text)) {
			return PALINDROME;
		} else {
			String faux = text.replaceAll("([A-Z])(\\1{1,})", "$1");
			StringBuilder fb = new StringBuilder(faux);
			
			if (fb.reverse().toString().equals(faux)) {
				return FAUX;
			}
		}
		
		return NOT_EVEN;
	}

}
